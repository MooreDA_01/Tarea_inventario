import os
from app.mercancia import Mercancia
if __name__ == "__main__":

    while True:
        inven = []

        print("\nREGRITRO DE MERCANCIA\n")
        print("1. Registrar ubicacion")
        print("2. Mostrar ubicacion")
        print("3. Agregar mercancia")
        print("4. Actualizar mercancia")
        print("5. Eliminar mercancia")
        print("6. Eliminar ubicacion")
        print("7. Salir")

        try:
            opcion = int(input("\n¿Que quieres hacer?: "))
        except:
            opcion = -1

        if opcion == 1:
            print("\n-- Registrar ubicacion --\n")
            n = input("Nombre: ")
            try:
                f = open(n + ".txt", "w+")
                f.close()
            except:
                print("ERROR: No se pudo registrar ubicacion.")

        elif opcion == 2:
            print("\n-- Mostrar ubicacion --\n")
            n = input("Nombre:")
            if os.path.exists(n + ".txt"):
                f = open(n + ".txt", "r")
                linea = f.readline()
                while linea:
                    data = linea.split(",")
                    print("* ", data[0] + " cantidad" + data[1])
                    linea = f.readline()
            else:
                    print("ERROR: Ubicacion introducida no existe.")

        elif opcion == 3:
            print("\n-- Agregar mercancia--\n")
            while True:
                n = input("Nombre del mercancia: ")
                m = input("Cantidad: ")
                inven.append(Mercancia(n, m))
                while True:
                    continuar = input("¿Quieres seguir agregando? (S/N): ").upper()
                    if continuar != "S" and continuar != "N":
                        print("Invalido, intenta de nuevo.")
                    else:
                        break
                if continuar == "N":
                    break
                elif continuar == "S":
                    pass
            while True:
                try:
                    n = input("¿Ubicacion?: ")
                    f = open(n + ".txt", "a")
                    for mer in inven:
                        f.write((str(mer.nombre)) + "," + str(mer.cantidad) + "\n")
                    f.close()
                    break
                except:
                    print("ERROR: No se encontro ubicacion.")

        elif opcion == 4:
            inv_temp = []
            print("\n--Actualizar mercancia--\n")

            while True:
                try:
                    n_est = input("Ubicacion: ")
                    f = open(n_est + ".txt", "r")
                    linea = f.readline()
                    i = 1
                    while linea:
                        inv_temp.append(linea)
                        data = linea.split(",")
                        print(str(i) + ". " + data[0])
                        linea = f.readline()
                        i += 1
                    f.close()
                    break
                except:
                    print("ERROR: No se encontro ubicacion.")

            while True:
                try:
                    mod = int(input("Numero de registro de mercancia: "))
                except:
                    mod = -1
                if mod > 0:
                    break
                else:
                    print("ERROR: No es correcto, no se encuentra.")
            n = input("\nNombre del mercancia: ")
            m = input("\ncantidad del mercancia: ")
            inv_temp[mod-1] = str(n) + "," + str(m) + "\n"

            while True:
                f = open(n_est + ".txt", "w")
                for prod in inv_temp:
                    f.write(prod)
                f.close()
                break
        elif opcion == 5:
            inv_temp = []
            print("\n-- Eliminar mercancia --\n")

            while True:
                try:
                    n_est = input("Ubicacion: ")
                    f = open(n_est + ".txt", "r")
                    linea = f.readline()
                    i = 1
                    while linea:
                        inv_temp.append(linea)
                        data = linea.split(",")
                        print(str(i) + ". " + data[0])
                        linea = f.readline()
                        i += 1
                    f.close()
                    break
                except:
                    print("ERROR: No se encontro ubicacion.")

            while True:
                try:
                    mod = int(input("Numero de la mercancia: "))
                except:
                    mod = -1
                if mod > 0:
                    break
                else:
                    print("ERROR: No es correcta o no existe.")

            inv_temp.pop(mod-1)

            while True:
                f = open(n_est + ".txt", "w")
                for prod in inv_temp:
                    f.write(prod)
                f.close()
                break


        elif opcion == 6:
            print("\n--Eliminar ubicacion--\n")
            n = input("\nUbicacion: ")
            if os.path.exists(n + ".txt"):
                os.remove(n + ".txt")
            else:
                print("ERROR: No se encuentra ubicacion.")

        elif opcion == 7:
            exit(0)
        else:
            print("Opcion invalida")